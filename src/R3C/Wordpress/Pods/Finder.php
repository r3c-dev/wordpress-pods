<?php
namespace R3C\Wordpress\Pods;

//Antiga PodsHelper
class Finder
{
    public static $params = array(
        'limit' => -1
    );

    /* Excluida
    public static function getField($pod, $fieldName)
    {
        return $pod->field($fieldName);
    }
    */

    //Antiga getPod
    public static function first($name, $filter = true)
    {
        $params = self::$params;

        if ( ! $filter) {
            $params['where'] = "t.post_status <> ''";
        }

        $pod = pods($name);
        $pod->find(self::$params);

        return $pod->fetch();
    }

    //Antiga getPods
    public static function get($name, $filter = true, $parameters = null)
    {
        $params = self::$params;

        if ( ! $filter) {
            $params['where'] = "t.post_status <> ''";
        }

        if ($parameters != null) {
            $params = array_merge($params, $parameters);
        }

        $pod = pods($name);
        return $pod->find($params);
    }

    //Antiga getPodsConfig
    public static function getConfig($name, $fieldName = null)
    {
        $params = self::$params;
        $params['where'] = '';

        $pod = pods($name);

        if ($fieldName) {
            $pod->find($params);
            $pod->fetch();
            return $pod->field($fieldName);
        }

        return $pod->find($params);
    }

    //Antiga get
    public static function getField($podName, $fieldName)
    {
        $pod = self::first($podName);
        return $pod->field($fieldName);
    }

    public static function getById($podName, $id)
    {
        $params = self::$params;
        $params['limit'] = 1;
        $params['where'] = 't.ID = ' . $id;

        $pod = pods($podName);

        return $pod->find($params);
    }
}